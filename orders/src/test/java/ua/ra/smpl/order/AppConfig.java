package ua.ra.smpl.order;

import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import ua.ra.smpl.order.service.PhoneService;

@Configuration
public class AppConfig {

    @Bean
    @Primary
    public PhoneService phoneService() {
        return Mockito.mock(PhoneService.class);
    }
}
