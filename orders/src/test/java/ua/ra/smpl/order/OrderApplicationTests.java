package ua.ra.smpl.order;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Mono;
import ua.ra.smpl.order.model.Order;
import ua.ra.smpl.order.model.Phone;
import ua.ra.smpl.order.repository.OrderRepository;
import ua.ra.smpl.order.service.PhoneService;

@AutoConfigureWebTestClient
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class OrderApplicationTests {
    @Autowired
    private WebTestClient webTestClient;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private PhoneService phoneService;

    @After
    public void clean() {
        orderRepository.deleteAll().subscribe();
    }

    @Before
    public void init() {
        orderRepository.deleteAll().subscribe();
    }

    @Test
    public void testOrderCreationFailedListsNotMatch() {
        Mockito.when(phoneService.retrievePhones(new Integer[]{2, 3, 4}))
               .thenReturn(
                   IntStream.of(2, 3, 4).mapToObj(this::makePhoneItem).collect(Collectors.toList()));

        List<Phone> phoneList = IntStream.range(1, 5).mapToObj(this::makePhoneItem).collect(Collectors.toList());
        Order order = new Order(1, "firstName", "lastName", "email", 16L, phoneList);

        webTestClient.post().uri("/")
                     .contentType(MediaType.APPLICATION_JSON_UTF8)
                     .body(Mono.just(order), Order.class)
                     .exchange()
                     .expectStatus()
                     .isEqualTo(HttpStatus.UNPROCESSABLE_ENTITY);
    }

    @Test
    public void testOrderCreationFailedTotalNotMatch() {
        Mockito.when(phoneService.retrievePhones(new Integer[]{2, 3, 4}))
               .thenReturn(
                   IntStream.of(2, 3, 4).mapToObj(this::makePhoneItem).collect(Collectors.toList()));

        List<Phone> phoneList = IntStream.of(2, 3, 4).mapToObj(this::makePhoneItem).collect(Collectors.toList());
        Order order = new Order(1, "firstName", "lastName", "email", 10L, phoneList);

        webTestClient.post().uri("/")
                     .contentType(MediaType.APPLICATION_JSON_UTF8)
                     .body(Mono.just(order), Order.class)
                     .exchange()
                     .expectStatus()
                     .isEqualTo(HttpStatus.UNPROCESSABLE_ENTITY);
    }

    @Test
    public void testOrderCreationSuccess() {
        Mockito.when(phoneService.retrievePhones(new Integer[]{2, 3, 4}))
               .thenReturn(
                   IntStream.of(2, 3, 4).mapToObj(this::makePhoneItem).collect(Collectors.toList()));

        List<Phone> phoneList = IntStream.of(2, 3, 4).mapToObj(this::makePhoneItem).collect(Collectors.toList());
        Order order = new Order(1, "firstNameOk", "lastName", "email", 9L, phoneList);

        webTestClient.post().uri("/")
                     .contentType(MediaType.APPLICATION_JSON_UTF8)
                     .body(Mono.just(order), Order.class)
                     .exchange()
                     .expectStatus().isOk()
                     .expectHeader().valueEquals("Content-Type", MediaType.APPLICATION_JSON_UTF8.toString())
                     .expectBody()
                     .jsonPath("$.firstName").isEqualTo("firstNameOk");
    }

    @Test
    public void testFindById() {
        List<Phone> phoneList = IntStream.of(4, 5).mapToObj(this::makePhoneItem).collect(Collectors.toList());
        Mockito.when(phoneService.retrievePhones(new Integer[]{4, 5}))
               .thenReturn(phoneList);
        Order order = new Order(17, "First Name 17", "lastName", "email", 9L, phoneList);
        webTestClient.post().uri("/")
                     .contentType(MediaType.APPLICATION_JSON_UTF8)
                     .body(Mono.just(order), Order.class)
                     .exchange();

        order = new Order(10, "First Name 10", "lastName", "email", 9L, phoneList);
        webTestClient.post().uri("/")
                     .contentType(MediaType.APPLICATION_JSON_UTF8)
                     .body(Mono.just(order), Order.class)
                     .exchange();

        webTestClient.get().uri("/17")
                     .exchange()
                     .expectStatus().isOk()
                     .expectHeader().valueEquals("Content-Type", MediaType.APPLICATION_JSON_UTF8.toString())
                     .expectBody()
                     .jsonPath("firstName").isEqualTo("First Name 17");
    }

    private Phone makePhoneItem(int id) {
        return new Phone(id, "name" + id, id, "description" + id, "imageUrl" + id);
    }
}
