package ua.ra.smpl.order.model;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Orders")
public class Order implements Serializable {

    @Id
    private Integer id;
    private String firstName;
    private String lastName;
    private String email;
    private Long total; // TODO: consider using Money class

    @Transient
    private List<Phone> phones;

    public Order() {}

    public Order(int id, String firstName, String lastName, String email, Long total, List<Phone> phones) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.total = total;
        this.phones = phones;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public List<Phone> getPhones() {
        return phones;
    }

    public void setPhones(List<Phone> phones) {
        this.phones = phones;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) { return true; }
        if (o == null || getClass() != o.getClass()) { return false; }
        Order order = (Order) o;
        return Objects.equals(id, order.id) &&
               Objects.equals(firstName, order.firstName) &&
               Objects.equals(lastName, order.lastName) &&
               Objects.equals(email, order.email) &&
               Objects.equals(total, order.total) &&
               Objects.equals(phones, order.phones);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, firstName, lastName, email, total, phones);
    }

    @Override
    public String toString() {
        return "Order{id=" + id + ", firstName='" + firstName + '\'' + ", lastName='" + lastName + '\'' +
               ", email='" + email + '\'' + ", total=" + total + ", phones=" + phones + '}';
    }
}
