package ua.ra.smpl.order.repository;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import ua.ra.smpl.order.model.Order;

public interface OrderRepository extends ReactiveMongoRepository<Order, Integer> {
}