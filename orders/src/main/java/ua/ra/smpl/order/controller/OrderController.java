package ua.ra.smpl.order.controller;

import java.util.List;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Mono;
import ua.ra.smpl.order.model.Order;
import ua.ra.smpl.order.model.Phone;
import ua.ra.smpl.order.repository.OrderRepository;
import ua.ra.smpl.order.service.PhoneService;
import ua.ra.smpl.order.utils.OrderValidator;

@RestController
public class OrderController {

    private static final Logger LOGGER = LoggerFactory.getLogger(OrderController.class);

    private final OrderRepository repository;

    private final PhoneService phoneService;

    @Autowired
    public OrderController(OrderRepository repository, PhoneService phoneService) {
        this.repository = repository;
        this.phoneService = phoneService;
    }

    @GetMapping("/{id}")
    public Mono<Order> findById(@PathVariable("id") int id) {
        LOGGER.info("findById: id={}", id);
        return repository.findById(id);
    }

    @PostMapping
    public Mono<Order> create(@RequestBody Order order) {
        LOGGER.info("create: {}", order);
        Integer[] ids = order.getPhones().stream()
                             .filter(Objects::nonNull)
                             .mapToInt(Phone::getId)
                             .boxed()
                             .toArray(Integer[]::new);
        List<Phone> phones = phoneService.retrievePhones(ids);
        if (OrderValidator.valid(order, phones)) {
            LOGGER.info("created: {}", order);
            return repository.save(order);
        }
        throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY);
    }
}
