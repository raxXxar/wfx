package ua.ra.smpl.order.utils;

import java.util.HashSet;
import java.util.List;
import java.util.Objects;

import ua.ra.smpl.order.model.Order;
import ua.ra.smpl.order.model.Phone;

public class OrderValidator {

    public static boolean valid(Order order, List<Phone> phones) {
        if (order.getPhones().size() != phones.size()) {
            return false;
        }
        if (!listEqualsIgnoreOrder(order.getPhones(), phones)) {
            return false;
        }
        return (order.getTotal() == calcTotal(order.getPhones()) && order.getTotal() == calcTotal(phones));
    }

    private static <T> boolean listEqualsIgnoreOrder(List<T> list1, List<T> list2) {
        return new HashSet<>(list1).equals(new HashSet<>(list2));
    }

    private static long calcTotal(List<Phone> phones) {
        return phones.stream()
                     .filter(Objects::nonNull)
                     .mapToLong(Phone::getPrice)
                     .filter(Objects::nonNull)
                     .sum();
    }
}
