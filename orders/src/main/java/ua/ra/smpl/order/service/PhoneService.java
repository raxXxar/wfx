package ua.ra.smpl.order.service;

import java.util.List;

import reactor.core.publisher.Flux;
import ua.ra.smpl.order.model.Phone;

public interface PhoneService {
    List<Phone> retrievePhones(Integer[] ids);
}
