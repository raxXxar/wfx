package ua.ra.smpl.order.model;

import java.util.Objects;

public class Phone {

    private int id;
    private String name;
    private Long price; // TODO: consider using Money class
    private String description;
    private String imageUrl;

    public Phone() {}

    public Phone(int id, String name, long price, String description, String imageUrl) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.description = description;
        this.imageUrl = imageUrl;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) { return true; }
        if (o == null || getClass() != o.getClass()) { return false; }
        Phone phone = (Phone) o;
        return id == phone.id &&
               Objects.equals(name, phone.name) &&
               Objects.equals(price, phone.price) &&
               Objects.equals(description, phone.description) &&
               Objects.equals(imageUrl, phone.imageUrl);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, name, price, description, imageUrl);
    }

    @Override
    public String toString() {
        return "Phone{id='" + id + '\'' + ", name='" + name + '\'' + ", price=" + price + ", description='" +
               description + '\'' + ", imageUrl='" + imageUrl + '\'' + '}';
    }
}
