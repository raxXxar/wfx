package ua.ra.smpl.order.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ua.ra.smpl.order.model.Phone;

@Service
public class PhoneServiceImpl implements PhoneService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PhoneServiceImpl.class);

    private final String phonesListEndpoint;

    private final WebClient webClient;

    @Autowired
    public PhoneServiceImpl(@Value("${phones-endpoint}") String phonesListEndpoint,
                            WebClient.Builder webClientBuilder) {
        this.phonesListEndpoint = phonesListEndpoint;
        this.webClient = webClientBuilder.build();
    }

    @Override
    public List<Phone> retrievePhones(Integer[] ids) {
        Flux<Phone> phones = webClient.post().uri(phonesListEndpoint)
                                      .body(Mono.just(ids), Integer[].class)
                                      .retrieve().bodyToFlux(Phone.class);
        return phones.collectList().block();
    }
}
