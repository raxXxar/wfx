package ua.ra.smpl.phone.controller;

import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ua.ra.smpl.phone.model.Phone;
import ua.ra.smpl.phone.repository.PhoneRepository;

@RestController
public class PhoneController {

    private static final Logger LOGGER = LoggerFactory.getLogger(PhoneController.class);

    private final PhoneRepository repository;

    @Autowired
    public PhoneController(PhoneRepository repository) {this.repository = repository;}

    @GetMapping("/{id}")
    public Mono<Phone> findById(@PathVariable("id") int id) {
        LOGGER.info("findById: id={}", id);
        return repository.findById(id);
    }

    @GetMapping
    public Flux<Phone> findAll() {
        LOGGER.info("findAll");
        return repository.findAll();
    }

    @PostMapping("/list")
    public Flux<Phone> findAllByIds(@RequestBody Integer[] ids) {
        List<Integer> list = Arrays.asList(ids);
        LOGGER.info("findByIds: ids={}", list);
        return repository.findAllById(list);
    }

    @PostMapping
    public Mono<Phone> create(@RequestBody Phone phone) {
        LOGGER.info("create: {}", phone);
        return repository.save(phone);
    }
}
