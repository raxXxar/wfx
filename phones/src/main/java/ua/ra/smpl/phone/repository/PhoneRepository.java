package ua.ra.smpl.phone.repository;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import ua.ra.smpl.phone.model.Phone;

public interface PhoneRepository extends ReactiveMongoRepository<Phone, Integer> {
}
