package ua.ra.smpl.phone;

import java.util.stream.IntStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Mono;
import ua.ra.smpl.phone.model.Phone;
import ua.ra.smpl.phone.repository.PhoneRepository;

@AutoConfigureWebTestClient
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class PhoneApplicationTests {
    @Autowired
    private WebTestClient webTestClient;

    @Autowired
    private PhoneRepository phoneRepository;

    @After
    public void clean() {
        phoneRepository.deleteAll().subscribe();
    }

    @Before
    public void init() {
        phoneRepository.deleteAll().subscribe();
        IntStream.range(1, 5).mapToObj(x -> new Phone(x, "name" + x, x * 1000 + 75, "description" + x, "imageUrl" + x))
                 .forEach(x -> webTestClient.post().uri("/")
                                            .contentType(MediaType.APPLICATION_JSON_UTF8)
                                            .body(Mono.just(x), Phone.class)
                                            .exchange());
    }

    @Test
    public void testFindById() {
        webTestClient.get().uri("/2")
                     .exchange()
                     .expectStatus().isOk()
                     .expectHeader().valueEquals("Content-Type", MediaType.APPLICATION_JSON_UTF8.toString())
                     .expectBody()
                     .jsonPath("name").isEqualTo("name2");
    }

    @Test
    public void testListByIds() {
        WebTestClient.ListBodySpec<Phone> phoneListBodySpec =
            webTestClient.post().uri("/list")
                         .body(Mono.just(new Integer[]{2, 4}), Integer[].class)
                         .exchange()
                         .expectStatus().isOk()
                         .expectHeader().valueEquals("Content-Type", MediaType.APPLICATION_JSON_UTF8.toString())
                         .expectBodyList(Phone.class);
        phoneListBodySpec.hasSize(2);
        phoneListBodySpec.contains(new Phone(2, "name2", 2075, "description2", "imageUrl2"),
                                   new Phone(4, "name4", 4075, "description4", "imageUrl4"));
    }

    @Test
    public void testFindAll() {
        webTestClient.get().uri("/")
                     .exchange()
                     .expectStatus().isOk()
                     .expectHeader().valueEquals("Content-Type", MediaType.APPLICATION_JSON_UTF8.toString())
                     .expectBodyList(Phone.class).hasSize(4);
    }

    @Test
    public void createPhone() {
        Phone phone = new Phone(100, "p.name", 75, "p.description", "p.imageUrl");
        webTestClient.post().uri("/")
                     .contentType(MediaType.APPLICATION_JSON_UTF8)
                     .body(Mono.just(phone), Phone.class)
                     .exchange()
                     .expectStatus().isOk()
                     .expectHeader().valueEquals("Content-Type", MediaType.APPLICATION_JSON_UTF8.toString())
                     .expectBody()
                     .jsonPath("$.name").isEqualTo("p.name");
    }
}
